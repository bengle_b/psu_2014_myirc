/*
** client_read.c for client_read in /home/bengle_b/rendu/PSU_2014_myirc
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Thu Apr  9 18:32:10 2015 Bengler Bastien
** Last update Fri Apr 10 16:58:02 2015 Bengler Bastien
*/

#include <stdio.h>
#include "myirc.h"

void	free_client(t_env *e, int fd)
{
  e->fd_type[fd] = FD_FREE;
  e->nickname[fd][0] = 0;
  e->channel[fd][0] = 0;
  e->fct_read[fd] = NULL;
}

int	client_read(t_env *e, int fd)
{
  char	buff[4096];
  int	r;

  if ((r = read(fd, buff, 4096)) == -1)
    {
      printf(ERROR_READ);
      return (-1);
    }
  if (r > 0)
    {
      buff[r] = '\0';
      parser(e, fd, buff);
    }
  else
    {
      printf("Client %s : disconnected\n", e->nickname[fd]);
      free_client(e, fd);
    }
  return (0);
}
