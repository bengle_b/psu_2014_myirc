/*
** cmd_nick.c for cmd_nick in /home/bengle_b/rendu/PSU_2014_myirc
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Fri Apr 10 17:45:04 2015 Bengler Bastien
** Last update Sat Apr 11 17:44:52 2015 Thomas Chartier
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "myirc.h"

static int	isAlreadyExist(t_env *e, char *nick, int fd)
{
  int	i;

  i = 0;
  while (i < MAX_FD)
    {
      if (strcmp(e->nickname[fd], nick) == 0)
	return (1);
      i++;
    }
  return (2);
}

static int	checkError(t_env *e, t_parser *parser, int fd)
{
  if (parser->args[0] == NULL ||
      strlen(parser->args[0]) == 0)
    {
      myWrite(fd, MSG_NONICKNAMEGIVEN);
      return (ERR_NONICKNAMEGIVEN);
    }
  if (isAlreadyExist(e, parser->args[0], fd) == 1
      && (e->nickname[fd] == NULL || strlen(e->nickname[fd]) == 0))
    {
      myWrite(fd, MSG_NICKCOLLISION);
      return (ERR_NICKCOLLISION);
    }
  if (isAlreadyExist(e, parser->args[0], fd) == 1
      && (e->nickname[fd] != NULL || strlen(e->nickname[fd]) == 0))
    {
      myWrite(fd, MSG_NICKNAMEINUSE);
      return (ERR_NICKNAMEINUSE);
    }
  return (1);
}

static int printName(int fd, char *msg, char *oldNick, char *newNick)
{
  char	*str;

  if (oldNick == NULL)
    {
      if ((str = malloc(sizeof(char) * 4096)) == NULL)
	return (-1);
      str = strcpy(str,msg);
      strcat(str, newNick);
      strcat(str, "\n");
      myWrite(fd,str);
      free(str);
    }
  else
    {
      if ((str = malloc(sizeof(char) * 4096)) == NULL)
	return (-1);
      str = strcpy(str,oldNick);
      strcat(str,msg);
      strcat(str, newNick);
      strcat(str, "\n");
      myWrite(fd,str);
    }
  return (1);
}

int	cmd_nick(t_env *e, t_parser *parser, int fd)
{
  int	error;

   if ((error = checkError(e, parser, fd)) != 1)
    {
      return (error);
    }
  if (e->nickname[fd] == NULL || strlen(e->nickname[fd]) == 0)
    {
      if (printName(fd, MSG_NEWNICK, NULL, parser->args[0]) == -1)
	{
	  printf(ERROR_MALLOC);
	  return (-1);
	}
    }
   else
    {
      if (printName(fd, MSG_CHANGENICK, e->nickname[fd], parser->args[0]) == -1)
	{
	  printf(ERROR_MALLOC);
	  return (-1);
	}
    }
  e->nickname[fd] = strcpy(e->nickname[fd], parser->args[0]);
  return (0);
}
