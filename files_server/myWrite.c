/*
** printError.c for printError in /home/charti_t/test/psu_2014_myirc/files_server
** 
** Made by Thomas Chartier
** Login   <charti_t@epitech.net>
** 
** Started on  Sat Apr 11 15:41:49 2015 Thomas Chartier
** Last update Sat Apr 11 17:16:40 2015 Bengler Bastien
*/

#include <stdio.h>
#include <string.h>
#include "myirc.h"

int	myWrite(int fd, char *str)
{
  if (write(fd, str, strlen(str)) == -1)
    {
      printf(ERROR_WRITE);
      return (-1);
    }
  return (0);
}
