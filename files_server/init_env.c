/*
** init_env.c for init_env in /home/bengle_b/rendu/PSU_2014_myirc
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Fri Apr 10 16:06:56 2015 Bengler Bastien
** Last update Sat Apr 11 17:46:49 2015 Bengler Bastien
*/

#include <stdlib.h>
#include <unistd.h>
#include "myirc.h"

void	init_list_channel(t_env *e)
{
  e->list_channel[0] = "epitech";
  e->list_channel[1] = "paris";
  e->list_channel[2] = "salade tomate oignon";
  e->list_channel[3] = "epice";
  e->list_channel[4] = "dragonnet";
  e->list_channel[5] = NULL;
}

int	init_pointer_function(t_env *e)
{
  e->list_cmd[0] = "/server";
  e->list_cmd[1] = "/nick";
  e->list_cmd[2] = "/list";
  e->list_cmd[3] = "/join";
  e->list_cmd[4] = "/part";
  e->list_cmd[5] = "/users";
  e->list_cmd[6] = "/msg";
  e->list_cmd[7] = NULL;
  e->fct_pointer[0] = NULL;
  e->fct_pointer[1] = &cmd_nick;
  e->fct_pointer[2] = NULL;
  e->fct_pointer[3] = &cmd_join;
  e->fct_pointer[4] = NULL;
  e->fct_pointer[5] = NULL;
  e->fct_pointer[6] = NULL;
  e->fct_pointer[7] = NULL;
  return (0);
}

char	*malloc_char(t_env *e)
{
  char	*tmp;

  if ((tmp = malloc(sizeof(char) * MAX_LENGTH)) == NULL)
    return (NULL);
  return (tmp);
}

char	**malloc_charchar(t_env *e, int size)
{
  char	**tmp;
  int	i;

  i = 0;
  if ((tmp = malloc(sizeof(char*) * MAX_LENGTH * MAX_CHAR)) == NULL)
    return (NULL);
  while (i < size)
    {
      if ((tmp[i] = malloc(sizeof(char) * MAX_CHAR)) == NULL)
	return (NULL);
      tmp[i][0] = 0;
      ++i;
    }
  return (tmp);
}

int	init_env(t_env *e)
{
  if ((e->fd_type = malloc_char(e)) == NULL)
    return (-1);
  if ((e->nickname = malloc_charchar(e, MAX_FD)) == NULL)
    return (-1);
  if ((e->channel = malloc_charchar(e, MAX_FD)) == NULL)
    return (-1);
  if ((e->list_cmd = malloc_charchar(e, 8)) == NULL)
    return (-1);
  if ((e->list_channel = malloc_charchar(e, 6)) == NULL)
    return (-1);
  init_pointer_function(e);
  init_list_channel(e);
  return (0);
}
