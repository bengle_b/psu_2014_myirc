/*
** init_free_parser.c for parser in /home/bengle_b/rendu/PSU_2014_myirc
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Fri Apr 10 15:52:52 2015 Bengler Bastien
** Last update Sat Apr 11 17:13:38 2015 Bengler Bastien
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "myirc.h"

int	init_parser(t_parser *parser)
{
  int	i;

  i = 0;
  if ((parser->cmd = malloc(sizeof(char) * MAX_LENGTH)) == NULL)
    return (-1);
  if ((parser->args = malloc(sizeof(char*) * MAX_LENGTH * MAX_CHAR)) == NULL)
    return (-1);
  while (i < MAX_LENGTH)
    {
      if ((parser->args[i] = malloc(sizeof(char) * MAX_CHAR)) == NULL)
	return (-1);
      parser->args[i][0] = 0;
      ++i;
    }
  parser->cmd[0] = 0;
  return (0);
}

int	free_parser(t_parser *parser)
{
  int	i;

  i = 0;
  free(parser->cmd);
  while (i < MAX_LENGTH)
    {
      free(parser->args[i]);
      ++i;
    }
  return (0);
}
