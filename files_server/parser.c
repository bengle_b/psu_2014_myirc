/*
** parser.c for parser in /home/bengle_b/rendu/PSU_2014_myirc
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Fri Apr 10 15:13:26 2015 Bengler Bastien
** Last update Sat Apr 11 18:24:08 2015 Bengler Bastien
*/

#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "myirc.h"

int		parse_buff(t_parser *parser, char *buff)
{
  const char	s[2] = " ";
  int		i;
  char		*tmp;

  i = 0;
  if ((tmp = strtok(buff, s)) == NULL)
    return (2);
  parser->cmd = strcpy(parser->cmd, tmp);
  while (tmp != NULL)
    {
      if ((tmp = strtok(NULL, s)) != NULL)
	{
	  parser->args[i] = strcpy(parser->args[i], tmp);
	  ++i;
	}
    }
  parser->args[i] = NULL;
  return (0);
}

int		research_cmd(t_parser *parser, t_env *e, int fd)
{
  int		i;

  i = 0;
  if (parser->cmd[0] != '/')
    return (1);
  while (e->list_cmd[i])
    {
      if (strcmp(e->list_cmd[i], parser->cmd) == 0)
	{
	  e->fct_pointer[i](e, parser, fd);
	  return (0);
	}
      ++i;
    }
  return (2);
}

int		parser(t_env *e, int fd, char *buff)
{
  t_parser	parser;
  int		res;

  if (buff[strlen(buff) - 1] == '\n')
    buff[strlen(buff) - 1] = '\0';
  if (init_parser(&parser) == -1)
    {
      printf("ERROR MALLOC\n");
      return (-1);
    }
  parse_buff(&parser, buff);
  if ((res = research_cmd(&parser, e, fd)) == 2)
    myWrite(fd, "421 - Unknow command.\n");
  else if (res == 1)
    send_message(e, &parser, fd);
  free_parser(&parser);
  return (0);
}
