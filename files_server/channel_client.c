/*
** channel_client.c for channel in /home/bengle_b/rendu/PSU_2014_myirc
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Fri Apr 10 15:09:55 2015 Bengler Bastien
** Last update Fri Apr 10 15:10:15 2015 Bengler Bastien
*/

#include "myirc.h"

void	channel_client(t_env *e, int fd, char *channel)
{
  int	i;

  i = 0;
  while (channel[i])
    {
      e->channel[fd][i] = channel[i];
      ++i;
    }
}
