/*
** cmd_join.c for join in /home/bengle_b/rendu/PSU_2014_myirc
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Sat Apr 11 16:26:40 2015 Bengler Bastien
** Last update Sat Apr 11 18:42:54 2015 Bengler Bastien
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "myirc.h"

static int	check_nb_arg(t_parser *parser)
{
  int		i;

  i = 0;
  while (parser->args[i])
    ++i;
  return (i);
}

static int	check_valid_channel(t_env *e, t_parser *parser)
{
  int		i;

  i = 0;
  while (e->list_channel[i])
    {
      if (strcmp(parser->args[0], e->list_channel[i]) == 0)
      	return (i);
      ++i;
    }
  return (-1);
}

static int	check_join_error(t_env *e, t_parser *parser, int fd)
{
  char	*msg;

  if (strlen(e->nickname[fd]) <= 0)
    {
      myWrite(fd, "Please specify a nickname before joining a channel.\n");
      return (-1);
    }
  if (check_nb_arg(parser) == 0)
    {
      myWrite(fd, "461 - /join : Not enough paramaters\n");
      return (-1);
    }
  if ((check_valid_channel(e, parser)) == -1)
    {
      if ((msg = malloc(sizeof(char) * 4096)) == NULL)
	return (-1);
      msg = strcpy(msg, "403 - ");
      strcat(msg, parser->args[0]);
      strcat(msg, " : No such channel\n");
      myWrite(fd, msg);
      free(msg);
      return (-1);
    }
  return (0);
}

int		cmd_join(t_env *e, t_parser *parser, int fd)
{
  int	error;
  char	*msg;

  if ((msg = malloc(sizeof(char) * 4096)) == NULL)
    return (-1);
  if (check_join_error(e, parser, fd) == 0)
    {
      msg = strcpy(msg, e->nickname[fd]);
      strcat(msg, " joined ");
      strcat(msg, parser->args[0]);
      strcat(msg, ".\n");
      myWrite(fd, msg);
      strcpy(e->channel[fd], parser->args[0]);
    }
  free(msg);
  return (0);
}
