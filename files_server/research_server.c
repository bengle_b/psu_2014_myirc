/*
** research_server.c for research_serv in /home/bengle_b/rendu/PSU_2014_myirc
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Fri Apr 10 14:37:29 2015 Bengler Bastien
** Last update Fri Apr 10 14:57:02 2015 Bengler Bastien
*/

#include <stdio.h>
#include <unistd.h>
#include "myirc.h"

int	clean_server(t_env *e)
{
  int	i;

  i = 0;
  while (i < MAX_FD)
    {
      if (e->fd_type[i] == 2)
	{
	  if (close(i) == -1)
	    {
	      printf("error close\n");
	      return (-1);
	    }
	  e->fd_type[i] = FD_FREE;
	  e->fct_read[i] = NULL;
	  if (close(i) == -1)
	    {
	      printf("error close\n");
	      return (-1);
	    }
	  return (0);
	}
      i++;
    }
  return (0);
}
