/*
** name_client.c for name_client in /home/bengle_b/rendu/PSU_2014_myirc
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Fri Apr 10 15:08:13 2015 Bengler Bastien
** Last update Fri Apr 10 15:17:09 2015 Bengler Bastien
*/

#include "myirc.h"

void	name_client(t_env *e, int fd, char *name)
{
  int	i;

  i = 0;
  while (name[i])
    {
      e->nickname[fd][i] = name[i];
      ++i;
    }
}
