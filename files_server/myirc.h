/*
** myirc.h for myir in /home/bengle_b/rendu/PSU_2014_myirc
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Thu Apr  9 16:12:53 2015 Bengler Bastien
** Last update Sat Apr 11 18:45:02 2015 Bengler Bastien
*/

#ifndef MYIRC_H_
# define MYIRC_H_

#include <sys/time.h>

typedef int			(*func_ptr)();

# define FD_FREE		0
# define FD_CLIENT		1
# define FD_SERVER		2
# define MAX_FD			255
# define MAX_CLIENT		255
# define MAX_LENGTH		4096
# define MAX_CHAR		255

# define USAGE			"./server [PORT]\n"
# define ERROR_SOCKET		"An error occured in the socket's creation.\n"
# define ERROR_BIND		"An error occured in the bind's function.\n"
# define ERROR_LISTEN		"An error occured in the listen's function.\n"
# define ERROR_READ		"An error occured in the read's function.\n"
# define ERROR_WRITE		"An error occured in the write's function.\n"
# define ERROR_MALLOC		"An error occured in the malloc function\n"

# define RPL_LISTSTART		321
# define RPL_LIST		322
# define RPL_LISTEND		323
# define RPL_NAMREPLY		353
# define RPL_ENDOFNAMES		366

# define ERR_NOSUCHNICK		401
# define ERR_NOSUCHSERVER	402
# define ERR_NOSUCHCHANNEL	403
# define ERR_NORECIPIENT	411
# define ERR_NOTEXTTOSEND	412
# define ERR_UNKNOWNCOMMAND	421
# define ERR_NONICKNAMEGIVEN	431
# define ERR_ERRONEUSNICKNAME	432
# define ERR_NICKNAMEINUSE	433
# define ERR_NICKCOLLISION	436
# define ERR_NOTONCHANNEL	442
# define ERR_NEEDMOREPARAMS	461
# define ERR_CHANNELISFULL	471

# define MSG_NOSUCHNICK		"No such nick/channel\n"
# define MSG_NOSUCHSERVER	"No such server\n"
# define MSG_NOSUCHCHANNEL	"No such channel\n"
# define MSG_NORECIPIENT	"No recipient given\n"
# define MSG_NOTEXTTOSEND	"No text to send\n"
# define MSG_NONICKNAMEGIVEN    "No nickname given\n"
# define MSG_ERRONEUSNICKNAME	"Erroneus nickname\n"
# define MSG_NICKNAMEINUSE	"Nickname is already in use\n"
# define MSG_NICKCOLLISION	"Nickname collision KILL\n"
# define MSG_NOTONCHANNEL	"You're not on that channel\n"
# define MSG_NEEDMOREPARAMS	"Not enough  parameters\n"
# define MSG_CHANNELISFULL	"Cannot join channel (+l)\n"

# define MSG_NEWNICK		"Introducing new nick "
# define MSG_CHANGENICK		" changed his nickname to "

# define WELCOME		"Welcome my friend.\nPlease specify your nickname.\n"

typedef struct			s_parser
{
  char				*cmd;
  char				**args;
}				t_parser;

typedef struct			s_env
{
  char				*fd_type;
  char				**nickname;
  char				**channel;
  char				**list_cmd;
  char				**list_channel;
  func_ptr			fct_pointer[8];
  func_ptr			fct_read[MAX_FD];
  func_ptr			fct_write[MAX_FD];
  struct timeval		tv;
}				t_env;

int				client_read(t_env *e, int fd);
int				server_read(t_env *e, int fd);
int				parser(t_env *e, int fd, char *buff);
int				cmd_nick(t_env *e, t_parser *parser, int fd);
int				cmd_join(t_env *e, t_parser *parser, int fd);

#endif /* !MYIRC_H_ */
