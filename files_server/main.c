/*
** main.c for main in /home/bengle_b/rendu/PSU_2014_myirc
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Thu Apr  9 16:12:31 2015 Bengler Bastien
** Last update Sat Apr 11 16:23:59 2015 Bengler Bastien
*/

#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include "myirc.h"

static int		create_socket(struct protoent *pe, int port)
{
  struct sockaddr_in	s_in;
  int			fd;

  if ((fd = socket(AF_INET, SOCK_STREAM, pe->p_proto)) == -1)
    {
      printf(ERROR_SOCKET);
      return (-1);
    }
  s_in.sin_family = AF_INET;
  s_in.sin_port = htons(port);
  s_in.sin_addr.s_addr = INADDR_ANY;
  if (bind(fd, (const struct sockaddr *)&s_in, sizeof(s_in)) == -1)
    {
      printf(ERROR_BIND);
      close(fd);
      return (-1);
    }
  if (listen(fd, MAX_CLIENT) == -1)
    {
      printf(ERROR_LISTEN);
      close(fd);
      return (-1);
    }
  return (fd);
}

static int		init_server(t_env *e, int port)
{
  int			fd;
  struct protoent	*pe;

  memset(e->fd_type, FD_FREE, MAX_FD);
  if ((pe = getprotobyname("TCP")) == NULL)
    {
      printf("error getprotobyname\n");
      return (-1);
    }
  if ((fd = create_socket(pe, port)) == -1)
    return (-1);
  e->fd_type[fd] = FD_SERVER;
  e->nickname[fd][0] = 0;
  e->channel[fd][0] = 0;
  e->fct_read[fd] = server_read;
  e->fct_write[fd] = NULL;
  e->tv.tv_sec = 2;
  e->tv.tv_usec = 40;
  return (0);
}

int			main(int ac, char **av)
{
  t_env			e;

  if (ac != 2)
    {
      printf(USAGE);
      return (-1);
    }
  init_env(&e);
  if (init_server(&e, atoi(av[1])) == -1)
    return (-1);
  if (loop_server(&e) == -1)
    {
      clean_server(&e);
      return (-1);
    }
  return (0);
}
