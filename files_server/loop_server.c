/*
** loop_server.c for loop_server in /home/bengle_b/rendu/PSU_2014_myirc
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Thu Apr  9 17:37:40 2015 Bengler Bastien
** Last update Sat Apr 11 15:04:50 2015 Bengler Bastien
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include "myirc.h"

static void		set_fd(t_env *e, fd_set *fd_read, fd_set *fd_write, int *fd_max)
{
  int			i;

  i = 0;
  while (i < MAX_FD)
    {
      if (e->fd_type[i] != FD_FREE)
	{
	  FD_SET(i, fd_read);
	  FD_SET(i, fd_write);
	  *fd_max = i;
	}
      ++i;
    }
}

static int		any_changes(fd_set *fd_read, fd_set *fd_write, t_env *e)
{
  int			i;

  i = 0;
  while (i < MAX_FD)
    {
      if (FD_ISSET(i, fd_read))
	{
	  if ((e->fct_read[i](e, i)) == -1)
	    {
	      printf("error any changes\n");
	      free_client(e, i);
	      return (-2);
	    }
	  prompt(e, i);
	}
      i++;
    }
  return (0);
}

int			loop_server(t_env *e)
{
  fd_set		fd_read;
  fd_set		fd_write;
  int			fd_max;

  while (1)
    {
      FD_ZERO(&fd_read);
      FD_ZERO(&fd_write);
      fd_max = 0;
      set_fd(e, &fd_read, &fd_write, &fd_max);
      if (select(fd_max + 1, &fd_read, &fd_write, NULL, &(e->tv)) == -1)
	{
	  printf("error select\n");
	  return (-1);
	}
      if (any_changes(&fd_read, &fd_write, e) == -1)
	return (-1);
    }
  return (0);
}
