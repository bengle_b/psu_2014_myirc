/*
** prompt.c for prompt in /home/bengle_b/rendu/PSU_2014_myirc
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Sat Apr 11 14:52:07 2015 Bengler Bastien
** Last update Sat Apr 11 15:05:30 2015 Bengler Bastien
*/

#include <string.h>
#include "myirc.h"

int	prompt(t_env *e, int fd)
{
  if (e->fd_type[fd] == 1 && (write(fd, "[", strlen("[")) == -1 ||
			     write(fd, e->nickname[fd], strlen(e->nickname[fd])) == -1 ||
			     write(fd, "]: ", strlen("]: ")) == -1))
    free_client(e, fd);
  return (0);
}
