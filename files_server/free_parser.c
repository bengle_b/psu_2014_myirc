/*
** free_parser.c for free_parser in /home/bengle_b/rendu/PSU_2014_myirc
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Fri Apr 10 16:48:25 2015 Bengler Bastien
** Last update Fri Apr 10 16:51:36 2015 Bengler Bastien
*/

#include <stdlib.h>
#include "myirc.h"

void	clear_parser(t_parser *parser)
{
  int	i;

  i = 0;
  parser->cmd[0] = 0;
  while (i < MAX_FD)
    {
      parser->args[i][0] = 0;
      ++i;
    }
}
