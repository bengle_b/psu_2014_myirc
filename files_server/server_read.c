/*
** server_read.c for server_read in /home/bengle_b/rendu/PSU_2014_myirc
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Fri Apr 10 14:21:40 2015 Bengler Bastien
** Last update Sat Apr 11 15:08:32 2015 Bengler Bastien
*/

#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include "myirc.h"

int			add_client(t_env *e, int fd)
{
  int			fd_client;
  struct sockaddr_in	client_sin;
  int			client_sin_len;

  client_sin_len = sizeof(client_sin);
  if ((fd_client = accept(fd, (struct sockaddr *)&client_sin, &client_sin_len)) == -1)
    {
      printf("error accept\n");
      return (-1);
    }
  e->fd_type[fd_client] = FD_CLIENT;
  e->nickname[fd_client][0] = 0;
  e->channel[fd_client][0] = 0;
  e->fct_read[fd_client] = client_read;
  e->fct_write[fd_client] = NULL;
  if (write(fd_client, WELCOME, strlen(WELCOME)) == -1)
    {
      printf(ERROR_WRITE);
      return (-1);
    }
  prompt(e, fd_client);
  return (0);
}

int			server_read(t_env *e, int fd)
{
  printf("New client \n");
  if (add_client(e, fd) == -1)
    return (-1);
  return (0);
}
