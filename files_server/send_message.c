/*
** send_message.c for send_message in /home/bengle_b/rendu/PSU_2014_myirc
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Sat Apr 11 18:23:40 2015 Bengler Bastien
** Last update Sat Apr 11 18:54:46 2015 Bengler Bastien
*/

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "myirc.h"

static int	check_error_s_msg(t_env *e, t_parser *parser, int fd)
{
  if (strlen(e->nickname[fd]) <= 0)
    {
      myWrite(fd, "431 - No nickname given.\n");
      return (-1);
    }
  else if (strlen(parser->cmd) <= 0)
    return (-1);
  else if (strlen(e->channel[fd]) <= 0)
    {
      myWrite(fd, "403 - No such channel.\n");
      return (-1);
    }
    return (0);
}

static int	printmsg(t_env *e, t_parser *parser, int fd, char *str)
{
  int		i;

  i = 0;
  while (i < MAX_FD)
    {
      if (i != fd && e->fd_type[i] == 1 &&
	  strcmp(e->channel[i], e->channel[fd]) == 0)
	myWrite(i, str);
      ++i;
    }
  return (0);
}

int		send_message(t_env *e, t_parser *parser, int fd)
{
  char		*str;
  int		i;

  i = 0;
  if ((str = malloc(sizeof(char) * 4096)) == NULL)
    return (-1);
  if (check_error_s_msg(e, parser, fd) == -1)
    return (-1);
  str = strcpy(str, "\n");
  strcat(str, e->nickname[fd]);
  strcat(str, ": ");
  str = strcat(str, parser->cmd);
  while (parser->args[i] != NULL)
    {
      strcat(str, " ");
      strcat(str, parser->args[i]);
      ++i;
    }
  strcat(str, "\n");
  printmsg(e, parser, fd, str);
  free(str);
  return (0);
}
