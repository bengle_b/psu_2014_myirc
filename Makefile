##
## Makefile for makefile in /home/bengle_b/rendu/PSU_2014_myirc
## 
## Made by Bengler Bastien
## Login   <bengle_b@epitech.net>
## 
## Started on  Wed Apr  8 16:10:43 2015 Bengler Bastien
## Last update Sat Apr 11 18:24:26 2015 Bengler Bastien
##


SRC_SERVER	= $(PATH_SERVER)/main.c \
		  $(PATH_SERVER)/loop_server.c \
		  $(PATH_SERVER)/client_read.c \
		  $(PATH_SERVER)/server_read.c \
		  $(PATH_SERVER)/clean_server.c \
		  $(PATH_SERVER)/parser.c \
		  $(PATH_SERVER)/init_free_parser.c \
		  $(PATH_SERVER)/init_env.c \
		  $(PATH_SERVER)/prompt.c \
		  $(PATH_SERVER)/cmd_nick.c \
		  $(PATH_SERVER)/cmd_join.c \
		  $(PATH_SERVER)/myWrite.c \
		  $(PATH_SERVER)/send_message.c

SRC_CLIENT	= $(PATH_CLIENT)/main.c \

OBJS_SERVER	= $(SRC_SERVER:.c=.o)

OBJS_CLIENT	= $(SRC_CLIENT:.c=.o)

CC		= gcc

NAME_SERVER	= server

NAME_CLIENT	= client

PATH_SERVER	= files_server

PATH_CLIENT	= files_client

RM		= rm -f

#CFLAGS		= -Wall -Wextra -Werror

all:	$(NAME_SERVER) #$(NAME_CLIENT)

$(NAME_SERVER):$(OBJS_SERVER)
	$(CC) $(OBJS_SERVER) -o $(NAME_SERVER)

$(NAME_CLIENT):$(OBJS_CLIENT)
	$(CC) $(OBJS_CLIENT) -o $(NAME_CLIENT)

clean:
	$(RM) $(OBJS_SERVER) $(OBJS_CLIENT)

fclean: clean
	$(RM) $(NAME_SERVER) $(NAME_CLIENT)

re: fclean all

.PHONY:	all clean fclean re
